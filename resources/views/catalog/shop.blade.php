@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @if ($data['settings']->shop_page_active)
        <section id="banner" class="__shop">
            <div class="container">
                <div class="wrapper-banner">
                    <h2>{!! $data['settings']->shop_page_banner_title !!}</h2>
                    <p>{{$data['settings']->shop_page_banner_text}}</p>
                    <a href="{{$data['settings']->shop_page_banner_link}}" target="_blank" class="btn">Перейти</a>
                    <img class="people-banner" src="/img/man-banner.png" alt="">
                </div>
            </div>
        </section>
        @include('partials.popular-goods', ['type' => 'shop-product'])
        <section id="catalog">
            <div class="container">
                <div class="row">
                    <div class="filter-outer">
                        <form class="products-filter" action="/catalog/shop/get-products-at-sub-categories">
                            <div class="__top">
                                <img src="/img/filter.svg" alt="">
                                <span>Каталог</span>
                            </div>
                            <div class="filter-groups">
                                @foreach($data['shop_categories'] as $category)
                                    <div class="group">
                                <span class="group-name active {{--{{ $loop->iteration == 1 ? 'active' : '' }}--}}">
                                    {{$category->name}}
                                </span>
                                        @foreach($category->sub_categories as $sub_category)
                                            <div class="filters">
                                                <label>
                                                    <input type="checkbox" name="sub_categories[]" value="{{$sub_category->id}}">
                                                    <span class="custom-checkbox"><img src="/img/bird.svg" alt=""></span>
                                                    <span class="filter-name">{{$sub_category->name}}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                            </div>
                        </form>
                    </div>
                    <div class="result-outer __ajax">
                        @include('partials.ajax.filtered-products', ['type' => 'shop-product'])
                    </div>
                </div>
            </div>
        </section>
    @else
        <div class="container" style="height: 50vh; padding-top: 100px">
            <h1>Страница еще в разработке !</h1>
        </div>
    @endif
@stop
