@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    @if ($data['settings']->rent_page_active)
        <section id="banner" class="__rent">
            <div class="container">
                <div class="wrapper-banner">
                    <h2>{!! $data['settings']->rent_page_banner_title !!}</h2>
                    <p>{{$data['settings']->rent_page_banner_text}}</p>
                    <img class="people-banner" src="/img/woman-banner.png" alt="">
                </div>
            </div>
        </section>
        @include('partials.popular-goods', ['type' => 'paint-tool-rental'])
        <section id="catalog">
            <div class="container">
                <div class="row">
                    <div class="filter-outer">
                        <form class="products-filter" action="/catalog/painting-tools-rental/get-products-at-sub-categories">
                            <div class="__top">
                                <img src="/img/filter.svg" alt="">
                                <span>Каталог</span>
                            </div>
                            <div class="filter-groups">
                                @foreach($data['painting_tool_categories'] as $category)
                                    <div class="group">
                                <span class="group-name active {{--{{ $loop->iteration == 1 ? 'active' : '' }}--}}">
                                    {{$category->name}}
                                </span>
                                        @foreach($category->sub_categories as $sub_category)
                                            <div class="filters">
                                                <label>
                                                    <input type="checkbox" name="sub_categories[]" value="{{$sub_category->id}}">
                                                    <span class="custom-checkbox"><img src="/img/bird.svg" alt=""></span>
                                                    <span class="filter-name">{{$sub_category->name}}</span>
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach

                            </div>
                        </form>
                    </div>
                    <div class="result-outer __ajax">
                        @include('partials.ajax.filtered-products', ['type' => 'paint-tool-rental'])
                    </div>
                </div>
            </div>
        </section>
    @else
        <div class="container" style="height: 50vh; padding-top: 100px">
            <h1>Страница еще в разработке !</h1>
        </div>
    @endif
@stop
