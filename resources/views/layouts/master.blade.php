<!DOCTYPE html>
@php $lang = App::getLocale() @endphp
<html lang="{{$lang}}">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta name="description" content="{{$data['seo']['description']}}">
        <meta name="keywords" content="{{$data['seo']['keywords']}}">

        <link rel="stylesheet" href="/css/locomotive-scroll.css">
        <link rel="stylesheet" href="/fonts/stylesheet.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="icon" type="image/x-icon" href="/img/logo.svg">

        <title>{{$data['seo']['title']}}</title>

        <!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-938371524"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-938371524'); </script>
        <!-- Event snippet for Website traffic conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-938371524/XY46CLTYqt8YEMTTub8D'}); </script>
    </head>
    <body>
    @php
        use App\Http\Resources\PaintingServiceResource;
        use App\Models\Painting_service;
        $settings = isset($data['settings']) ? $data['settings'] : \App\Models\Setting::first();
        $painting_services = isset($data['painting_services']) ? $data['painting_services'] : resourceData(
            PaintingServiceResource::collection(Painting_service::where('active', 1)
            ->orderBy('sort', 'desc')
            ->get())
        );
    @endphp
        <header>
            <div class="container">
                <a href="/"><img class="logo" src="/{{$settings->logo}}" alt=""></a>
                <nav>
                    <ul>
                        <li><a href="/about-us">О нас</a></li>
                        <li><a href="#our-services" class="anchor">Малярные услуги</a></li>
                        <li><a href="/catalog/shop">Интернет магазин</a></li>
                        <li><a href="/catalog/painting-tools-rental">Аренда инструмента</a></li>
                        <li><a href="/contact">Контакты</a></li>
                    </ul>
                </nav>
                <div class="burger-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </header>
        <div id="content" class="smooth-scroll" data-scroll-container>
            @yield('content')
            <footer>
                <div class="container">
                    <div class="row __top">
                        <div class="group">
                            <span class="name">Наши услуги</span>
                            <nav class="__services">
                                <ul>
                                    @foreach($painting_services as $service)
                                        <li><a href="#our-services" class="anchor">{{$service->name}}</a></li>
                                    @endforeach
                                </ul>
                            </nav>
                        </div>
                        <div>
                            <nav class="__center">
                                <ul>
                                    <li><a href="/about-us">О нас</a></li>
                                    <li><a href="#our-services" class="anchor">Малярные услуги</a></li>
                                    <li><a href="/catalog/shop">Интернет магазин</a></li>
                                    <li><a href="/catalog/painting-tools-rental">Аренда инструмента</a></li>
                                    <li><a href="/contact">Контакты</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div>
                            <div class="btn dark to-modal-request">Оставить заявку</div>
                            <div class="row groups">
                                <div class="col-50 group">
                                    <span class="name">Телефон</span>
                                    @foreach(json_decode($settings->phone_list) as $item)
                                        <a href="tel: {{cleanPhone($item->phone)}}" class="value">{{$item->phone}}</a>
                                    @endforeach
                                </div>
                                <div class="col-50 group">
                                    <span class="name">Email</span>
                                    @foreach(json_decode($settings->email_list) as $item)
                                        <a href="mailto: {{$item->email}}" class="value">{{$item->email}}</a>
                                    @endforeach
                                </div>
                                <div class="col-100 group">
                                    <span class="name">Наши соц. сети</span>
                                    <div class="social-links_horizontal">
                                        @if($settings->youtube_link)
                                            <a href="{{$settings->youtube_link}}" target="_blank"><img src="/img/youtube-icon.svg" alt=""></a>
                                        @endif
                                        @if($settings->whatsapp_link)
                                            <a href="{{$settings->whatsapp_link}}" target="_blank"><img src="/img/whatsapp-icon.svg" alt=""></a>
                                        @endif
                                        @if($settings->linkedIn_link)
                                            <a href="{{$settings->linkedIn_link}}" target="_blank"><img src="/img/in-icon.svg" alt=""></a>
                                        @endif
                                        @if($settings->facebook_link)
                                            <a href="{{$settings->facebook_link}}" target="_blank"><img src="/img/facebook-icon.svg" alt=""></a>
                                        @endif
                                        @if($settings->instagram_link)
                                            <a href="{{$settings->instagram_link}}" target="_blank"><img src="/img/instagram.svg" alt=""></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="all-rights">All rights reserved AQQU ENERGY</span>
                </div>
            </footer>
        </div>
        <div class="overlay-modals" >
            <div class="modal product-item" id="modal-shop-product">
                <img class="close" src="/img/close.svg" alt="">
                <div class="wrapper __ajax"></div>
            </div>
            <div class="modal modal-form" id="modal-paint-tool-rental">
                <img class="close" src="/img/close.svg" alt="">
                <h2 class="ta-center">Оставить заявку</h2>
                <form action="/application/send-painting-tool-rental" method="post" class="default row">
                    <input type="hidden" value="" required name="tool">
                    <div class="col-50">
                        <label>
                            <input type="text" name="name" placeholder="Имя">
                        </label>
                    </div>
                    <div class="col-50">
                        <label>
                            <input class="input-phone" type="text" name="phone" placeholder="Телефон" required>
                        </label>
                    </div>
                    <div class="col-100">
                        <label>
                            <input type="text" name="rental-period" placeholder="Срок аренды инструмента" required>
                        </label>
                    </div>
                    <div class="col-100">
                        <button class="btn" type="submit">Отправить</button>
                    </div>
                </form>
            </div>
            <div class="modal modal-form" id="modal-request">
                <img class="close" src="/img/close.svg" alt="">
                <h2 class="ta-center">Оставить заявку</h2>
                <form action="/application/send-application" method="post" class="default row">
                    <input type="hidden" value="" required name="tool">
                    <div class="col-50">
                        <label>
                            <input type="text" name="name" placeholder="Имя" required>
                        </label>
                    </div>
                    <div class="col-50">
                        <label>
                            <input class="input-phone" type="text" name="phone" placeholder="Телефон" required>
                        </label>
                    </div>
                    <div class="col-100">
                        <button class="btn" type="submit">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
        @mobile
        <div class="menu__wrapper-block">
            <nav>
                <ul>
                    <li><a href="/about-us">О нас</a></li>
                    <li><a href="#our-services" class="anchor">Малярные услуги</a></li>
                    <li><a href="/catalog/shop">Интернет магазин</a></li>
                    <li><a href="/catalog/painting-tools-rental">Аренда инструмента</a></li>
                    <li><a href="/contact">Контакты</a></li>
                </ul>
            </nav>
            <div class="socials-wrapper">
                <span class="name">Наши соц. сети</span>
                <div class="social-links_horizontal">
                    @if($settings->youtube_link)
                        <a href="{{$settings->youtube_link}}" target="_blank"><img class="img-svg" src="/img/youtube-icon.svg" alt=""></a>
                    @endif
                    @if($settings->whatsapp_link)
                        <a href="{{$settings->whatsapp_link}}" target="_blank"><img class="img-svg"  src="/img/whatsapp-icon.svg" alt=""></a>
                    @endif
                    @if($settings->linkedIn_link)
                        <a href="{{$settings->linkedIn_link}}" target="_blank"><img class="img-svg"  src="/img/in-icon.svg" alt=""></a>
                    @endif
                    @if($settings->facebook_link)
                        <a href="{{$settings->facebook_link}}" target="_blank"><img class="img-svg"  src="/img/facebook-icon.svg" alt=""></a>
                    @endif
                    @if($settings->instagram_link)
                        <a href="{{$settings->instagram_link}}" target="_blank"><img class="img-svg"  src="/img/instagram.svg" alt=""></a>
                    @endif
                </div>
            </div>
            <div class="btn dark to-modal-request">Оставить заявку</div>
        </div>
        @endmobile

        <div type="button" class="email-btn to-modal-request">
            <div class="text-send">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <span>Обратная<br>связь</span>
            </div>
        </div>

        <script src="/js/CustomEase.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollTrigger.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollToPlugin.min.js"></script>
        <script src="/js/locomotive-scroll.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
        <script src="/js/jquery.maskedinput.min.js"></script>
        <script src="/js/script.js"></script>
    </body>
</html>
