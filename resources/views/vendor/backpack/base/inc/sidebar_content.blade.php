<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('mainslide') }}'><i class='nav-icon la la-list'></i> Main Slides</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('weworkitem') }}'><i class='nav-icon la la-list'></i> How We Work</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('painting_service') }}'><i class='nav-icon las la-list'></i> Painting Services</a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-layer-group"></i>Shop Products</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shopcategory') }}'><i class='nav-icon las la-list'></i> ShopCategories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shopsubcategory') }}'><i class='nav-icon las la-list'></i> ShopSubCategories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shopproduct') }}'><i class='nav-icon las la-list'></i> ShopProducts</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-layer-group"></i>Painting Tools </a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('paintingtoolcategory') }}'><i class='nav-icon las la-list'></i> Paint.ToolCat.</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('paintingtoolsubcategory') }}'><i class='nav-icon las la-list'></i> Paint.ToolSubCat.</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('paintingtoolproduct') }}'><i class='nav-icon las la-list'></i> Paint.ToolProducts</a></li>
    </ul>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('partner') }}'><i class='nav-icon las la-list'></i> Partners</a></li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('seoinfo') }}'><i class='nav-icon la la-sitemap'></i> Seo infos</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cogs'></i> Settings</a></li>
