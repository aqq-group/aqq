@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    <section id="main_top-section">
        <div class="swiper" id="swiper-main_slides">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    @foreach($data['main_slides'] as $slide)
                        <img class="main-top-bg" src="/{{$slide->img_tmb}}" alt="" data-scroll data-scroll-speed="-2">
                        <div class="container">
                            <h1>{!! $slide->title !!}</h1>
                            <p>{{$slide->text}}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container container-socials">
            <div class="social-links">
                @if($data['settings']->youtube_link)
                    <a href="{{$data['settings']->youtube_link}}" target="_blank"><img src="/img/youtube-icon.svg" alt=""></a>
                @endif
                @if($data['settings']->whatsapp_link)
                    <a href="{{$data['settings']->whatsapp_link}}" target="_blank"><img src="/img/whatsapp-icon.svg" alt=""></a>
                @endif
                @if($data['settings']->linkedIn_link)
                    <a href="{{$data['settings']->linkedIn_link}}" target="_blank"><img src="/img/in-icon.svg" alt=""></a>
                @endif
                @if($data['settings']->facebook_link)
                    <a href="{{$data['settings']->facebook_link}}" target="_blank"><img src="/img/facebook-icon.svg" alt=""></a>
                @endif
                @if($data['settings']->instagram_link)
                    <a href="{{$data['settings']->instagram_link}}" target="_blank"><img src="/img/instagram.svg" alt=""></a>
                @endif
            </div>
        </div>
    </section>
    <section id="about-us">
        <div class="container">
            <div class="row">
                <div>
                    <span class="text-line">О нас</span>
                    <h2 class="up-fade_animation">Мы - команда <br> опытных профессионалов</h2>
                    <p>
                        Выбирая нас, вы выбираете надежного партнера, готового воплотить
                        в жизнь любые идеи и задумки. Мы создаем не просто красивые стены,
                        а атмосферное пространство, которое будет вас окружать радостью
                        и комфортом. С нами ваш проект будет реализован
                        мастерски - от идеи до реальности.
                    </p>
                    <a href="/about-us" class="btn">Подробнее</a>
                </div>
                <div>
                    <img data-scroll data-scroll-speed="2" src="/img/about-us.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <section id="our-services">
        <div class="container">
            <div class="__top">
                <div>
                    <span class="text-line">наши услуги</span>
                    <h2 class="up-fade_animation">Малярные услуги</h2>
                </div>
                <p>Мы предоставляем широкий спектр малярных услуг, чтобы удовлетворить все ваши потребности. Мы предлагаем:</p>
            </div>
            <div class="swiper" id="swiper-services">
                <div class="swiper-wrapper">
                    @foreach( $data['painting_services'] as $service)
                        <div class="swiper-slide service-item">
                            <div class="img-outer">
                                <img src="{{$service->img_tmb}}" alt="">
                            </div>
                            <h3>{{$service->name}}</h3>
                            <p>{{$service->text}}</p>
                        </div>
                    @endforeach
                </div>
                <div class="button-nav button-prev"><img src="/img/arrow-prev.svg" alt=""></div>
                <div class="button-nav button-next"><img src="/img/arrow-next.svg" alt=""></div>
            </div>
            <div class="btn to-modal-request">Оставить заявку</div>
        </div>
    </section>
    <section id="how-we-work">
        <div class="container">
            <h2 class="ta-center __big up-fade_animation">Как мы работаем?</h2>
            <div class="row">
                @foreach($data['we_work_items'] as $item)
                    <div class="up-fade_animation">
                        <span class="num">0{{$loop->iteration}}</span>
                        <h3>{{$item->title}}</h3>
                        <p>{{$item->text}}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section id="shop">
        <div class="container">
            <h2 class="up-fade_animation">Наш интернет магазин</h2>
            <p>
                Мы являемся вашим надежным партнером в мире профессиональных малярных решений.
                У нас вы найдете широкий выбор инструментов высокого качества, предназначенных для различных задач, связанных с покраской и отделкой.
            </p>
            <div class="swiper" id="swiper-products">
                <div class="swiper-wrapper">
                    @foreach($data['shop_main_page_products'] as $product)
                        <div class="swiper-slide">
                            @include('partials.product-card', ['type' => 'shop-product'])
                        </div>
                    @endforeach
                </div>
                <div class="button-nav button-prev"><img src="/img/arrow-prev.svg" alt=""></div>
                <div class="button-nav button-next"><img src="/img/arrow-next.svg" alt=""></div>
            </div>
            <a href="/catalog/shop" class="btn show-all">Показать все товары</a>
        </div>
    </section>
    <section id="paint-shop-rent">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="__left">
                        <h2 class="up-fade_animation">аренда малярных инструментов!</h2>
                        <p>Мы понимаем, что выполнение малярных работ может быть временным проектом, и иногда покупка инструментов может показаться излишней тратой. Именно поэтому мы предлагаем вам удобный и экономичный способ получить доступ к необходимым инструментам без лишних затрат.</p>
                        <a href="/catalog/painting-tools-rental" class="btn show-all">Показать все инструменты</a>
                    </div>
                    <div class="__right">
                        <div class="swiper" id="swiper_paint-shop-rent">
                            <div class="swiper-wrapper">
                                @foreach($data['painting_tools_main_page_products'] as $product)
                                    <div class="swiper-slide">
                                        @include('partials.product-card', ['type' => 'paint-tool-rental'])
                                    </div>
                                @endforeach
                            </div>
                            <div class="button-nav-outer">
                                <div class="button-nav-long button-prev"><img src="/img/arrow-prev-long.svg" alt=""></div>
                                <div class="button-nav-long button-next"><img src="/img/arrow-next-long.svg" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="trust-us">
        <div class="container">
            <div class="row">
                <div>
                    <span class="text-line">Наши клиенты</span>
                </div>
                <div>
                    <h2 class="up-fade_animation">Те кто нам доверяют</h2>
                    <p>
                        Мы гордимся тем, что сотрудничаем с широким спектром клиентов.
                        Наше партнерство с ними - это источник вдохновения и постоянного роста.
                        Вот лишь некоторые из наших клиентов, которые доверили нам свои проекты:
                    </p>
                </div>
            </div>
            <div class="swiper" id="swiper_trust-us">
                <div class="swiper-wrapper">
                    @foreach($data['partners'] as $partner)
                        <div class="swiper-slide">
                            <img src="{{$partner->img}}" alt="">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section id="contact-us">
        @include('partials.contact')
    </section>
@stop
