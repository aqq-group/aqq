@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    <section id="contact-us" class="__page">
        @include('partials.contact', ['contact_page'=>true])
    </section>
@stop
