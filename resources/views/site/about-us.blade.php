@extends('layouts.master')

@section('content')
    @php $lang = App::getLocale() @endphp
    <section id="about-us__page">
        <div class="container">
            <span class="text-line">О нас</span>
            <div class="row __top">
                <div>
                    <h2 class="up-fade_animation __top">{!! $data['settings']->about_us_top_title !!}</h2>
                    <div>{!! $data['settings']->about_us_top_description !!}</div>
                </div>
                <div>
                    <img data-scroll data-scroll-speed="2" src="/{{tmbPath($data['settings']->about_us_top_img)}}" alt="">
                </div>
            </div>
            <div class="__html">
                {!! $data['settings']->about_us_html !!}
            </div>
        </div>
    </section>
@stop
