@foreach($data['products_at_category'] as $key => $products)
    <div class="group">
        <h2 class="group-name">{{$key}}</h2>
        <div class="row items">
            @foreach($products as $product)
                <div class="col-25">
                    @include('partials.product-card')
                </div>
            @endforeach
        </div>
    </div>
@endforeach
