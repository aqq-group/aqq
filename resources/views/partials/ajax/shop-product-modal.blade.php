<div class="__top">
    <img class="product-img" src="/{{$data['shop_product']->img_tmb}}" alt="">
    <div>
        <span class="name">{{$data['shop_product']->name}}</span>
        <span class="price">{{$data['shop_product']->price}} тг</span>
        <p class="short-description">{{$data['shop_product']->short_description}}</p>
        <a href="{{$data['shop_product']->link}}" target="_blank" class="btn">Приобрести</a>
    </div>
</div>
<div class="description-outer">
    <span class="description-title">Описание:</span>
    <div class="html-text">{!! $data['shop_product']->description !!}</div>
</div>
