<div class="container">
    @if (isset($contact_page))
        <h1>Наши контакты</h1>
    @endif
    <div class="row row-outer">
        <div class="__left">
            <h2 class="up-fade_animation">Свяжитесь <br>с нами!</h2>
            <div class="groups">
                <div>
                    <h3>Телефон</h3>
                    @foreach(json_decode($data['settings']->phone_list) as $item)
                        <a href="tel: {{cleanPhone($item->phone)}}" class="value">{{$item->phone}}</a>
                    @endforeach
                </div>
                <div>
                    <h3>eMail</h3>
                    @foreach(json_decode($data['settings']->email_list) as $item)
                        <a href="mailto: {{$item->email}}" class="value">{{$item->email}}</a>
                    @endforeach
                </div>
                <div>
                    <h3>Адрес</h3>
                    <span class="value">{{$data['settings']->address}}</span>
                </div>
            </div>
        </div>
        <div class="__right">
            <h2 class="up-fade_animation">У вас <br>остались вопросы?</h2>
            <form action="/application/send-question" method="post" class="default row">
                <div class="col-50">
                    <label>
                        <input type="text" name="name" placeholder="Имя" required>
                    </label>
                </div>
                <div class="col-50">
                    <label>
                        <input class="input-phone" type="text" name="phone" placeholder="Телефон" required>
                    </label>
                </div>
                <div class="col-100">
                    <label>
                        <span>Ваше сообщение</span>
                        <textarea name="message" required></textarea>
                    </label>
                </div>
                <div class="col-btn">
                    <button class="btn" type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>
