<section id="popular-goods">
    <div class="container">
        <h2 class="up-fade_animation">Популярные товары</h2>
        <div class="swiper" id="swiper-products_popular">
            <div class="swiper-wrapper">
                @foreach($data['popular_products'] as $product)
                    <div class="swiper-slide">
                        @include('partials.product-card', ['type' => $type])
                    </div>
                @endforeach
            </div>
            <div class="button-nav button-prev"><img src="/img/arrow-prev.svg" alt=""></div>
            <div class="button-nav button-next"><img src="/img/arrow-next.svg" alt=""></div>
        </div>
    </div>
</section>
