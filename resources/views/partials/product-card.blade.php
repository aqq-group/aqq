<div class="product-item">
    <img src="/{{$product->img_tmb}}" alt="">
    <span class="name">{{$product->name}}</span>
    <p class="short-description">{{$product->short_description}}</p>
    <span class="price">{{$product->price}} тг @if($type == 'paint-tool-rental') / сутки@endif</span>
    <div class="btn"
         data-attr-id="{{$product->id}}"
         data-attr-name="{{$product->name}}"
         data-attr-type="{{$type}}"
    >
        Подробнее
    </div>
</div>
