<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('shop_sub_category_id');
            $table->foreign('shop_sub_category_id')
                ->references('id')->on('shop_sub_categories')->onDelete('cascade');

            $table->string('name');
            $table->string('short_description');
            $table->text('description');
            $table->integer('price');
            $table->string('link')->nullable();

            $table->boolean('popular_show')->default(false);
            $table->boolean('main_page_show')->default(false);

            $table->string('img', '500')->nullable();

            $table->integer('sort')->nullable();
            $table->boolean('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_products');
    }
}
