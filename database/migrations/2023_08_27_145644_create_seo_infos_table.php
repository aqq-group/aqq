<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_infos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('main_page_title', '255')->nullable();
            $table->string('main_page_description', '255')->nullable();
            $table->string('main_page_keywords', '255')->nullable();

            $table->string('shop_page_title', '255')->nullable();
            $table->string('shop_page_description', '255')->nullable();
            $table->string('shop_page_keywords', '255')->nullable();

            $table->string('rent_page_title', '255')->nullable();
            $table->string('rent_page_description', '255')->nullable();
            $table->string('rent_page_keywords', '255')->nullable();

            $table->string('about_us_page_title', '255')->nullable();
            $table->string('about_us_page_description', '255')->nullable();
            $table->string('about_us_page_keywords', '255')->nullable();

            $table->string('contact_page_title', '255')->nullable();
            $table->string('contact_page_description', '255')->nullable();
            $table->string('contact_page_keywords', '255')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_infos');
    }
}
