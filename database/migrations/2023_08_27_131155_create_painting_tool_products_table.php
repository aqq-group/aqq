<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaintingToolProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('painting_tool_products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->unsignedBigInteger('painting_tool_sub_category_id');
            $table->foreign('painting_tool_sub_category_id')
                ->references('id')->on('painting_tool_sub_categories')->onDelete('cascade');

            $table->string('name');
            $table->string('short_description');
            $table->integer('price');

            $table->boolean('popular_show')->default(false);
            $table->boolean('main_page_show')->default(false);

            $table->string('img', '500')->nullable();

            $table->integer('sort')->nullable();
            $table->boolean('active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('painting_tool_products');
    }
}
