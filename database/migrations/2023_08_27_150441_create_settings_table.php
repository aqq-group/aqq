<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('logo', '500')->nullable();

            $table->boolean('shop_page_active')->default(true);
            $table->string('shop_page_banner_title')->nullable();
            $table->string('shop_page_banner_text', 500)->nullable();
            $table->string('shop_page_banner_link')->nullable();

            $table->boolean('rent_page_active')->default(true);
            $table->string('rent_page_banner_title')->nullable();
            $table->string('rent_page_banner_text', 500)->nullable();
            $table->string('rent_page_banner_link')->nullable();

            $table->string('about_us_top_title')->nullable();
            $table->text('about_us_top_description')->nullable();
            $table->string('about_us_top_img')->nullable();
            $table->text('about_us_html')->nullable();

            $table->string('address', 500);

            $table->text('email_list')->nullable();
            $table->text('phone_list')->nullable();

            $table->string('youtube_link')->nullable();
            $table->string('whatsapp_link')->nullable();
            $table->string('linkedIn_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('instagram_link')->nullable();

            $table->string('telegram_link')->nullable();
            $table->string('tiktok_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('vk_link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
