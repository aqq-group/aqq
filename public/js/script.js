let body = $('body');
let mobile = false;

if ($(window).width() < 992){
    mobile = true;
}

/*Locomotive with Scrolltrigger*/
gsap.registerPlugin(ScrollTrigger, ScrollToPlugin);

// Using Locomotive Scroll from Locomotive https://github.com/locomotivemtl/locomotive-scroll
const locoScroll = new LocomotiveScroll({
    el: document.querySelector(".smooth-scroll"),
    smooth: true
});
// each time Locomotive Scroll updates, tell ScrollTrigger to update too (sync positioning)
locoScroll.on("scroll", ScrollTrigger.update);

// tell ScrollTrigger to use these proxy methods for the ".smooth-scroll" element since Locomotive Scroll is hijacking things
ScrollTrigger.scrollerProxy(".smooth-scroll", {
    scrollTop(value) {
        return arguments.length ? locoScroll.scrollTo(value, 0, 0) : locoScroll.scroll.instance.scroll.y;
    }, // we don't have to define a scrollLeft because we're only scrolling vertically.
    getBoundingClientRect() {
        return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
    },
    // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
    pinType: document.querySelector(".smooth-scroll").style.transform ? "transform" : "fixed"
});

ScrollTrigger.addEventListener("refresh", () => locoScroll.update());
ScrollTrigger.refresh();

/*GSAP ANIMATION*/
const titles = gsap.utils.toArray('.up-fade_animation');
titles.forEach(title => {
    gsap.to(title, {
        scrollTrigger: {
            scroller: (!mobile ? '.smooth-scroll' : ''),
            trigger: title,
            start: "top-=100 bottom",
            onEnter: () => {
                title.classList.add("scrolled");
            },
            onLeaveBack: self => self.disable()
        }
    })
});

const images = gsap.utils.toArray('.img-animation');
images.forEach(image => {
    gsap.to(image, {
        scrollTrigger: {
            scroller: (!mobile ? '.smooth-scroll' : ''),
            trigger: image,
            start: "top+=50% bottom",
            onEnter: () => {
                image.classList.add("scrolled");
            },
            onLeaveBack: self => self.disable()
        }
    })
});

const texts = gsap.utils.toArray('.text-animation');
texts.forEach(text => {
    gsap.to(text, {
        scrollTrigger: {
            scroller: (!mobile ? '.smooth-scroll' : ''),
            trigger: text,
            start: "top+=30% bottom",
            onEnter: () => {
                text.classList.add("scrolled");
            },
            onLeaveBack: self => self.disable()
        }
    })
});


$(function(){
    $(".input-phone").mask("+9(999) 999-99-99");
});

$("img.img-svg").each(function () {
    var $img = $(this);
    var imgClass = $img.attr("class");
    var imgURL = $img.attr("src");
    $.get(imgURL, function (data) {
        var $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
            $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
            $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"))
        }
        $img.replaceWith($svg);
    }, "xml");
});

$('.anchor').click(function (e) {
    e.preventDefault();
    let elemId = $(this).attr('href');
    if ($(elemId).length) {
        locoScroll.scrollTo(elemId);
    }else{
        window.location.href = "/" + elemId;
    }
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/*Ajax forms*/
$("form.default").submit(function (e) {
    e.preventDefault();
    $('.btn[type="submit"]').prop('disabled', true);
    $.ajax({
        url: $(this).attr('action'),
        type: "POST",
        data: $(this).serialize(),
        success: function(response) {
            $('.btn[type="submit"]').prop('disabled', false);
            alert(response);
            $('.overlay-modals').fadeOut();
        },
        error: function(response) {
            $('.btn[type="submit"]').prop('disabled', false);
            console.log(response);
            alert('Ошибка!');
        }
    });
});

$('.overlay-modals .modal').click(function (e) {
    e.stopPropagation();
});

$('.overlay-modals, .modal .close').click(function () {
    $('.overlay-modals').fadeOut();
});

/*Overlay-menu*/
$('.menu__wrapper-block nav a').click(function () {
    $('.burger-menu, .menu__wrapper-block').removeClass('opened');
});

$(".burger-menu").click(function () {
    $(".burger-menu, .menu__wrapper-block").toggleClass("opened");
    if ($(this).hasClass('opened')){
        $('body').addClass('overflow-hidden');
    }else{
        $('body').removeClass('overflow-hidden');
    }
});

/*Swipers*/
var swiper_services = new Swiper("#swiper-services", {
    slidesPerView: 3,
    spaceBetween: 35,
    navigation: {
        prevEl: '#swiper-services .button-prev',
        nextEl: '#swiper-services .button-next',
    },
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    speed: 500,
    breakpoints: {
        991: {
            slidesPerView: 1.25,
            spaceBetween: 25,
        }
    }
});

var swiper_products = new Swiper("#swiper-products", {
    slidesPerView: 5,
    spaceBetween: 15,
    navigation: {
        prevEl: '#swiper-products .button-prev',
        nextEl: '#swiper-products .button-next',
    },
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    speed: 500,
    breakpoints: {
        991: {
            slidesPerView: 1.75,
            spaceBetween: 25,
        }
    }
});

var swiper_products_popular = new Swiper("#swiper-products_popular", {
    slidesPerView: 6,
    spaceBetween: 45,
    navigation: {
        prevEl: '#swiper-products_popular .button-prev',
        nextEl: '#swiper-products_popular .button-next',
    },
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    speed: 500,
    breakpoints: {
        991: {
            slidesPerView: 1.75,
            spaceBetween: 25,
        }
    }
});

var swiper_paint_shop_rent = new Swiper("#swiper_paint-shop-rent", {
    slidesPerView: 2,
    spaceBetween: 15,
    navigation: {
        prevEl: '#swiper_paint-shop-rent .button-prev',
        nextEl: '#swiper_paint-shop-rent .button-next',
    },
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    speed: 500,
    breakpoints: {
        991: {
            slidesPerView: 1.25,
            spaceBetween: 25,
        }
    }
});

var swiper_trust_us = new Swiper("#swiper_trust-us", {
    slidesPerView: 4,
    slidesPerColumn: 2,
    spaceBetween: 15,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    speed: 500,
    breakpoints: {
        991: {
            slidesPerView: 1.75,
            slidesPerColumn: 1,
        }
    }
});

var swiper_main_slides = new Swiper("#swiper-main_slides", {
    slidesPerView: 1,
    autoplay: {
        delay: 3500,
        disableOnInteraction: false,
    },
    speed: 500,
    effect: 'fade',
    loop: true
});

//initFilter
let filterGroupName = $('.filter-groups .group-name.active')
filterGroupName.next('.filters').slideDown(function () {
    locoScroll.update();
    ScrollTrigger.refresh();
})

$('.filter-groups .group-name').click(function () {
    $(this).toggleClass('active')
    if ($(this).hasClass('active')){
        $(this).next('.filters').slideDown(function () {
            locoScroll.update();
            ScrollTrigger.refresh();
        })
    }else{
        $(this).next('.filters').slideUp(function () {
            locoScroll.update();
            ScrollTrigger.refresh();
        })
    }
})

$('form.products-filter input').change(function () {
    let url = $('form.products-filter').attr('action')
    let sub_categories_ids = [];
    $("form.products-filter input[name='sub_categories[]']:checked").each(function() {
        sub_categories_ids.push($(this).val());
    });

    $.post(url, {sub_categories_ids}, function(data){
        $('.result-outer.__ajax').html(data)
    });
})

$('body').on('click', '.product-item .btn', function () {
    $('.overlay-modals .modal').css('display','none')
    if ($(this).attr('data-attr-type') === 'shop-product'){
        $.get("/catalog/shop/get-product-modal", {id: $(this).attr('data-attr-id')}, function(data){
            $('#modal-shop-product .wrapper.__ajax').html(data)
            $('#modal-shop-product').css('display', 'block')
            $('.overlay-modals').css("display", "flex")
                .hide()
                .fadeIn();
        });
    }else{
        if($(this).attr('data-attr-type') === 'paint-tool-rental'){
            $("#modal-paint-tool-rental input[name='tool']").val($(this).attr('data-attr-name'))
            $('#modal-paint-tool-rental').css('display', 'block')
            $('.overlay-modals').css("display", "flex")
                .hide()
                .fadeIn();
        }
    }
})

$('.to-modal-request').click(function () {
    $('.overlay-modals .modal').css('display','none')
    $('#modal-request').css('display', 'block')
    $('.overlay-modals').css("display", "flex")
        .hide()
        .fadeIn();
})

/*FULL LOAD*/
window.onload = function() {
    setTimeout(function() {
        locoScroll.update();
        ScrollTrigger.refresh();
    }, 200);
    $(".my-intro").addClass('close');
    $('html').addClass('loaded');
};
