<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('painting_service', 'Painting_serviceCrudController');
    Route::crud('shopcategory', 'ShopCategoryCrudController');
    Route::crud('shopsubcategory', 'ShopSubCategoryCrudController');
    Route::crud('shopproduct', 'ShopProductCrudController');
    Route::crud('paintingtoolcategory', 'PaintingToolCategoryCrudController');
    Route::crud('paintingtoolsubcategory', 'PaintingToolSubCategoryCrudController');
    Route::crud('paintingtoolproduct', 'PaintingToolProductCrudController');
    Route::crud('partner', 'PartnerCrudController');
    Route::crud('seoinfo', 'SeoInfoCrudController');
    Route::crud('setting', 'SettingCrudController');
    Route::crud('weworkitem', 'WeWorkItemCrudController');
    Route::crud('mainslide', 'MainSlideCrudController');
}); // this should be the absolute last line of this file
