<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\front\CatalogController;
use App\Http\Controllers\front\SiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(SiteController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/about-us', 'aboutUs');
    Route::get('/contact', 'contact');
});

Route::controller(CatalogController::class)->group(function () {
    Route::get('/catalog/shop', 'shop');
    Route::post('/catalog/shop/get-products-at-sub-categories', 'getShopProductsAtSubCategories');
    Route::get('/catalog/shop/get-product-modal', 'getShopProductModal');

    Route::get('/catalog/painting-tools-rental', 'paintingToolsRental');
    Route::post('/catalog/painting-tools-rental/get-products-at-sub-categories', 'getPaintingToolProductsAtSubCategories');
});

Route::controller(ApplicationController::class)->group(function () {
    Route::post('/application/send-application', 'sendApplication');
    Route::post('/application/send-question', 'sendQuestion');
    Route::post('/application/send-painting-tool-rental', 'sendPaintingToolRental');
});
