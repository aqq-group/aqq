<?php

namespace App\Models;

use App\Traits\CustomCrudTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use CustomCrudTrait {
        setImgAttribute as setImgAttributeTrait;
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'settings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        parent::boot();

        static::deleting(function($obj) {
            $disk = config('backpack.base.root_disk_name');

            \Storage::disk($disk)->delete('public/' . $obj->logo);

            \Storage::disk($disk)->delete('public/' . $obj->about_us_top_img);
            \Storage::disk($disk)->delete('public/' . str_replace('uploads/', 'uploads/tmb/', $obj->about_us_top_img));
        });
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setLogoAttribute($value)
    {
        $this->setFileAttribute($value, 'logo');
    }

    public function setAboutUsTopImgAttribute($value)
    {
        $this->setImgAttributeTrait($value, '720','about_us_top_img');
    }
}
