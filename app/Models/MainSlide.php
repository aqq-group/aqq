<?php

namespace App\Models;

use App\Traits\CustomCrudTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class MainSlide extends Model
{
    use CustomCrudTrait {
        setImgAttribute as setImgAttributeTrait;
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'main_slides';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        parent::boot();

        static::deleting(function($obj) {
            $disk = config('backpack.base.root_disk_name');

            \Storage::disk($disk)->delete('public/' . $obj->img);
            \Storage::disk($disk)->delete('public/' . str_replace('uploads/', 'uploads/tmb/', $obj->img));
        });
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setImgAttribute($value)
    {
        $this->setImgAttributeTrait($value, 1920);
    }
}
