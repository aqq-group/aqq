<?php

namespace App\Models;

use App\Traits\CustomCrudTrait;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class PaintingToolProduct extends Model
{
    use CustomCrudTrait {
        setImgAttribute as setImgAttributeTrait;
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'painting_tool_products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function boot()
    {
        parent::boot();

        static::deleting(function($obj) {
            $disk = config('backpack.base.root_disk_name');

            \Storage::disk($disk)->delete('public/' . $obj->img);
            \Storage::disk($disk)->delete('public/' . str_replace('uploads/', 'uploads/tmb/', $obj->img));
        });
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */


    public function paintingToolSubCategory()
    {
        return $this->belongsTo(PaintingToolSubCategory::class, 'painting_tool_sub_category_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setImgAttribute($value)
    {
        $this->setImgAttributeTrait($value, 500);
    }
}
