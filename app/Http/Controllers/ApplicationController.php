<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApplicationPaintingToolRentalRequset;
use App\Http\Requests\ApplicationQuestionRequest;
use App\Http\Requests\ApplicationRequest;
use App\Http\Requests\ReviewApplicationRequest;
use App\Http\Requests\MailFaqRequest;
use App\Http\Requests\MailQuestionRequest;
use App\Http\Requests\MailReviewRequest;
use App\Mail\ApplicationMail;
use App\Mail\ApplicationPaintingToolRentalMail;
use App\Mail\ApplicationQuestionMail;
use App\Mail\SendReviewMail;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class ApplicationController extends Controller
{
    public function sendApplication(ApplicationRequest $request){
        $arr = array(
            'Имя' => $request->post('name'),
            'Телефон' => $request->post('phone'),
        );

        //telegram
        $token =  env('TELEGRAM_TOKEN');
        $chat_id = env('TELEGRAM_CHAT_ID');
        $txt = "<b>Заявка</b>%0A";
        foreach($arr as $key => $value) {
            $txt .= "<b>".$key."</b> ".$value."%0A";
        }
        /*try {
            $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");
        }catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }*/

        //email
        Mail::to(env('TO_EMAIL'))->send(new ApplicationMail($request));

        return "Заявка успешно отправлена!";
    }

    public function sendQuestion(ApplicationQuestionRequest $request){
        $arr = array(
            'Имя' => $request->post('name'),
            'Телефон' => $request->post('phone'),
            'Сообщение' => $request->post('message'),
        );

        //telegram
        $token =  env('TELEGRAM_TOKEN');
        $chat_id = env('TELEGRAM_CHAT_ID');
        $txt = "<b>Заявка (Остались вопросы)</b>%0A";
        foreach($arr as $key => $value) {
            $txt .= "<b>".$key."</b> ".$value."%0A";
        }
        /*try {
            $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");
        }catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }*/

        //email
        Mail::to(env('TO_EMAIL'))->send(new ApplicationQuestionMail($request));

        return "Заявка успешно отправлена!";
    }

    public function sendPaintingToolRental(ApplicationPaintingToolRentalRequset $request){
        $arr = array(
            'Имя' => $request->post('name'),
            'Телефон' => $request->post('phone'),
            'Инструмент' => $request->post('tool'),
            'Срок аренды инструмента' => $request->post('rental-period'),
        );

        //telegram
        $token =  env('TELEGRAM_TOKEN');
        $chat_id = env('TELEGRAM_CHAT_ID');
        $txt = "<b>Заявка (Аренда малярного инструмента)</b>%0A";
        foreach($arr as $key => $value) {
            $txt .= "<b>".$key."</b> ".$value."%0A";
        }
        /*try {
            $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");
        }catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }*/

        //email
        Mail::to(env('TO_EMAIL'))->send(new ApplicationPaintingToolRentalMail($request));

        return "Заявка успешно отправлена!";
    }
}
