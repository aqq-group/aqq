<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PaintingToolProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PaintingToolProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaintingToolProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\PaintingToolProduct');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/paintingtoolproduct');
        $this->crud->setEntityNameStrings('paintingtoolproduct', 'painting_tool_products');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'img',
            'label' => "Image",
            'type' => 'image',
            'height' => '50px',
        ]);

        $this->crud->addColumn([
            'name'      => 'paintingToolSubCategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
        ]);

        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->removeColumn('painting_tool_sub_category_id');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PaintingToolProductRequest::class);

        $this->crud->addField([
            'type' => 'select2_grouped',
            'label' => 'SubCategory',

            'name' => 'painting_tool_sub_category_id', // имя поля
            'entity' => 'paintingToolSubCategory', // имя сущности, с которой вы работаете
            'attribute' => 'name', // атрибут, который будет отображаться в выпадающем списке

            'group_by' => 'paintingToolCategory', // Используйте group_by для группировки по имени категории
            'group_by_attribute' => 'name', // the attribute on related model, that you want shown
            'group_by_relationship_back' => 'paintingToolSubCategories', // relationship from related model back to this model
        ]);

        $this->crud->addField([
            'label' => 'Image',
            'name' => "img",
            'type' => 'image',
            'crop' => true,
            'aspect_ratio' => 0,
        ]);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
