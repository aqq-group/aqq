<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SettingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SettingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Setting');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/setting');
        $this->crud->setEntityNameStrings('setting', 'settings');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'logo',
            'type'      => 'image',
            'height' => '50px'
        ]);


        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SettingRequest::class);

        $this->crud->addField([   // Upload
            'name'      => 'logo',
            'type'      => 'upload',
            'upload'    => true,
            //'disk'      => 'uploads', // if you store files in the /public folder, please omit this; if you store them in /storage or S3, please specify it;
            // optional:
            'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URLs this will make a URL that is valid for the number of minutes specified
        ]);


        $this->crud->addField([
            'name' => 'address',
            'type' => 'textarea',
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'email_list',
            'label' => 'Email list',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'email',
                    'type'    => 'text',
                    'label'   => 'Email',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
            ],

            // optional
            'new_item_label'  => 'Add Email', // customize the text of the button
            'init_rows' => 1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
        ]);

        $this->crud->addField([   // repeatable
            'name'  => 'phone_list',
            'label' => 'Phone list',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'name'    => 'phone',
                    'type'    => 'text',
                    'label'   => 'Phone',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ],
            ],

            // optional
            'new_item_label'  => 'Add Phone', // customize the text of the button
            'init_rows' => 1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
        ]);

        $this->crud->addField([
            'name' => 'shop_page_active',
            'type' => 'checkbox',
            'tab'  => 'Shop Page',
        ]);
        $this->crud->addField([
            'name' => 'shop_page_banner_title',
            'tab'  => 'Shop Page',
        ]);
        $this->crud->addField([
            'name' => 'shop_page_banner_text',
            'type' => 'textarea',
            'tab'   => 'Shop Page',
        ]);
        $this->crud->addField([
            'name' => 'shop_page_banner_link',
            'tab'   => 'Shop Page',
        ]);

        $this->crud->addField([
            'name' => 'rent_page_active',
            'type' => 'checkbox',
            'tab'  => 'Rent Page',
        ]);
        $this->crud->addField([
            'name' => 'rent_page_banner_title',
            'tab'  => 'Rent Page',
        ]);
        $this->crud->addField([
            'name' => 'rent_page_banner_text',
            'type' => 'textarea',
            'tab'   => 'Rent Page',
        ]);
        $this->crud->addField([
            'name' => 'rent_page_banner_link',
            'tab'   => 'Rent Page',
        ]);

        $this->crud->addField([
            'name' => 'about_us_top_title',
            'tab'   => 'About Us Page',
        ]);
        $this->crud->addField([   // CKEditor
            'name'          => 'about_us_top_description',
            'type'          => 'ckeditor',
            'options'       => [
                'autoGrow_minHeight'   => 200,
                'autoGrow_bottomSpace' => 50,
                'removePlugins'        => 'resize,maximize',
            ],
            'tab'   => 'About Us Page',
        ],);
        $this->crud->addField([
            'name' => 'about_us_top_img',
            'type' => 'image',
            'height' => '50px',
            'tab'   => 'About Us Page',
        ]);
        $this->crud->addField([   // CKEditor
            'name'          => 'about_us_html',
            'type'          => 'ckeditor',
            'options'       => [
                'autoGrow_minHeight'   => 200,
                'autoGrow_bottomSpace' => 50,
                'removePlugins'        => 'resize,maximize',
            ],
            'tab'   => 'About Us Page',
        ],);

        $this->crud->addField([
            'name'            => 'youtube_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'facebook_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'instagram_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'linkedIn_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'telegram_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'tiktok_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'twitter_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'vk_link',
            'tab'             => 'SOCIALS',
        ]);
        $this->crud->addField([
            'name'            => 'whatsapp_link',
            'tab'             => 'SOCIALS',
        ]);


        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
