<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SeoInfoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SeoInfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SeoInfoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\SeoInfo');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/seoinfo');
        $this->crud->setEntityNameStrings('seoinfo', 'seo_infos');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SeoInfoRequest::class);

        $this->crud->addField([
            'name'            => 'main_page_title',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'main_page_description',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'main_page_keywords',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);

        $this->crud->addField([
            'name'            => 'shop_page_title',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'shop_page_description',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'shop_page_keywords',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);

        $this->crud->addField([
            'name'            => 'rent_page_title',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'rent_page_description',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'rent_page_keywords',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);

        $this->crud->addField([
            'name'            => 'about_us_page_title',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'about_us_page_description',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'about_us_page_keywords',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);

        $this->crud->addField([
            'name'            => 'contact_page_title',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'contact_page_description',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);
        $this->crud->addField([
            'name'            => 'contact_page_keywords',
            'type'            => 'textarea',
            'wrapper' => ['class' => 'form-group col-md-4'],
        ]);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
