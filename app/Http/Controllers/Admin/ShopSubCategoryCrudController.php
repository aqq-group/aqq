<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ShopSubCategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ShopSubCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShopSubCategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\ShopSubCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/shopsubcategory');
        $this->crud->setEntityNameStrings('shopsubcategory', 'shop_sub_categories');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'shopCategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
        ]);

        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->removeColumn('shop_category_id');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ShopSubCategoryRequest::class);

        $this->crud->addField([  // Select
            'type'      => 'select',
            'name'      => 'shop_category_id', // the db column for the foreign key

            // optional
            // 'entity' should point to the method that defines the relationship in your Model
            'entity'    => 'shopCategory',

            // optional - manually specify the related model and attribute
            'attribute' => 'name', // foreign key attribute that is shown to user
        ],);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
