<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Painting_serviceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class Painting_serviceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Painting_serviceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Painting_service');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/painting_service');
        $this->crud->setEntityNameStrings('painting_service', 'painting_services');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'img',
            'label' => "Image",
            'type' => 'image',
            'height' => '50px',
        ]);

        // TODO: remove setFromDb() and manually define Columns, maybe Filters
       $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(Painting_serviceRequest::class);

        $this->crud->addField([
            'label' => 'Image',
            'name' => "img",
            'type' => 'image',
            'crop' => true,
            'aspect_ratio' => 0,
        ]);

        $this->crud->addField([
            'name' => "text",
            'type' => 'textarea',
        ]);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
