<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PaintingToolSubCategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PaintingToolSubCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaintingToolSubCategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\PaintingToolSubCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/paintingtoolsubcategory');
        $this->crud->setEntityNameStrings('paintingtoolsubcategory', 'painting_tool_sub_categories');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'paintingToolCategory', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
        ]);

        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->removeColumn('painting_tool_category_id');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PaintingToolSubCategoryRequest::class);

        $this->crud->addField([  // Select
            'type'      => 'select',
            'name'      => 'painting_tool_category_id', // the db column for the foreign key

            // optional
            // 'entity' should point to the method that defines the relationship in your Model
            'entity'    => 'paintingToolCategory',

            // optional - manually specify the related model and attribute
            'attribute' => 'name', // foreign key attribute that is shown to user
        ],);

        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
