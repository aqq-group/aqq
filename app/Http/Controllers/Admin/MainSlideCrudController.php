<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MainSlideRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MainSlideCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MainSlideCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\MainSlide');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/mainslide');
        $this->crud->setEntityNameStrings('mainslide', 'main_slides');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'img',
            'label' => "Image",
            'type' => 'image',
            'height' => '50px',
        ]);

        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(MainSlideRequest::class);

        $this->crud->addField([
            'name' => 'text',
            'type' => 'textarea',
        ]);

        $this->crud->addField([
            'label' => 'Image',
            'name' => "img",
            'type' => 'image',
            'crop' => true,
            'aspect_ratio' => 0,
        ]);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
