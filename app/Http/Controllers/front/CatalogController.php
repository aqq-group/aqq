<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Http\Resources\PaintingToolCategoryResource;
use App\Http\Resources\PaintingToolProductResource;
use App\Http\Resources\ShopCategoryResource;
use App\Http\Resources\ShopProductResource;
use App\Models\PaintingToolCategory;
use App\Models\PaintingToolProduct;
use App\Models\SeoInfo;
use App\Models\ShopCategory;
use App\Models\ShopProduct;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function shop(){
        $data['popular_products'] = resourceData(ShopProductResource::collection(ShopProduct::where('active', 1)
            ->where('popular_show', 1)
            ->orderBy('sort', 'desc')->get()
        ));
        $data['shop_categories'] = resourceData(ShopCategoryResource::collection(ShopCategory::where('active', 1)
            ->with('shopSubCategories')
            ->orderBy('sort', 'desc')->get()
        ));

        $shop_all_products = ShopProduct::where('active', 1)
            ->orderBy('sort', 'desc')->get();
        $data['products_at_category'] = [];
        foreach ($shop_all_products as $product){
            $data['products_at_category'][$product->shopSubCategory->shopCategory->name][] = resourceData(new ShopProductResource($product));
        }

        $data['settings'] = \App\Models\Setting::first();
        $seo_info = SeoInfo::first();
        $data['seo']['title'] = $seo_info->{'shop_page_title'};
        $data['seo']['description'] =$seo_info->{'shop_page_description'};
        $data['seo']['keywords'] =  $seo_info->{'shop_page_keywords'};

        return view('catalog.shop', compact('data'));
    }

    public function getShopProductsAtSubCategories(Request $request){
        $query = ShopProduct::query();
        if ($request->sub_categories_ids) {
            $query->whereIn('shop_sub_category_id', $request->sub_categories_ids);
        }
        $filtered_products = $query
            ->where('active', 1)
            ->orderBy('sort', 'desc')
            ->get();

        $data['products_at_category'] = [];
        foreach ($filtered_products as $product){
            $data['products_at_category'][$product->shopSubCategory->shopCategory->name][] = resourceData(new ShopProductResource($product));
        }

        $type = 'shop-product';
        return view('partials.ajax.filtered-products', compact('data', 'type'))->render();
    }

    public function getShopProductModal(Request $request){
        $data['shop_product'] = resourceData(new ShopProductResource(ShopProduct::where('active', 1)
            ->where('id', $request->id)
            ->first()
        ));

        return view('partials.ajax.shop-product-modal', compact('data'))->render();
    }

    //'________________________________'

    public function paintingToolsRental(){
        $data['popular_products'] = resourceData(PaintingToolProductResource::collection(PaintingToolProduct::where('active', 1)
            ->where('popular_show', 1)
            ->orderBy('sort', 'desc')->get()
        ));

        $data['painting_tool_categories'] = resourceData(PaintingToolCategoryResource::collection(PaintingToolCategory::where('active', 1)
            ->with('paintingToolSubCategories')
            ->orderBy('sort', 'desc')->get()
        ));

        $painting_tool_all_products = PaintingToolProduct::where('active', 1)
            ->orderBy('sort', 'desc')->get();
        $data['products_at_category'] = [];
        foreach ($painting_tool_all_products as $product){
            $data['products_at_category'][$product->paintingToolSubCategory->paintingToolCategory->name][] = resourceData(new PaintingToolProductResource($product));
        }

        $data['settings'] = \App\Models\Setting::first();
        $seo_info = SeoInfo::first();
        $data['seo']['title'] = $seo_info->{'rent_page_title'};
        $data['seo']['description'] =$seo_info->{'rent_page_description'};
        $data['seo']['keywords'] =  $seo_info->{'rent_page_keywords'};

        return view('catalog.painting-tools-rental', compact('data'));
    }

    public function getPaintingToolProductsAtSubCategories(Request $request){
        $query = PaintingToolProduct::query();
        if ($request->sub_categories_ids) {
            $query->whereIn('painting_tool_sub_category_id', $request->sub_categories_ids);
        }

        $filtered_products = $query
            ->where('active', 1)
            ->orderBy('sort', 'desc')
            ->get();

        $data['products_at_category'] = [];
        foreach ($filtered_products as $product){
            $data['products_at_category'][$product->paintingToolSubCategory->paintingToolCategory->name][] = resourceData(new PaintingToolProductResource($product));
        }

        $type = 'paint-tool-rental';
        return view('partials.ajax.filtered-products', compact('data', 'type'))->render();
    }
}
