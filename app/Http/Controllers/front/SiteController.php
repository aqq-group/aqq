<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Http\Resources\DelicacyResource;
use App\Http\Resources\FaqResource;
use App\Http\Resources\HamResource;
use App\Http\Resources\MainSlideResource;
use App\Http\Resources\PaintingServiceResource;
use App\Http\Resources\PaintingToolProductResource;
use App\Http\Resources\ReviewResource;
use App\Http\Resources\ShopProductResource;
use App\Http\Resources\WeWorkItemResource;
use App\Models\Delicacy;
use App\Models\Faq;
use App\Models\Ham;
use App\Models\MainSlide;
use App\Models\Painting_service;
use App\Models\PaintingToolProduct;
use App\Models\Partner;
use App\Models\Review;
use App\Models\SeoInfo;
use App\Models\ShopProduct;
use App\Models\WeWorkItem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class SiteController extends Controller
{
    public function index(){
        //Artisan::call('config:cache');
        $data = [];

        $data['painting_services'] = resourceData(PaintingServiceResource::collection(Painting_service::where('active', 1)->orderBy('sort', 'desc')->get()));
        $data['shop_main_page_products'] =  resourceData(ShopProductResource::collection(ShopProduct::where('active', 1)
            ->where('main_page_show', 1)
            ->orderBy('sort', 'desc')->get()
        ));
        $data['painting_tools_main_page_products'] =  resourceData(PaintingToolProductResource::collection(PaintingToolProduct::where('active', 1)
            ->where('main_page_show', 1)
            ->orderBy('sort', 'desc')->get()
        ));
        $data['we_work_items'] = resourceData(WeWorkItemResource::collection(WeWorkItem::where('active', 1)->orderBy('sort', 'asc')->get()));
        $data['main_slides'] = resourceData(MainSlideResource::collection(MainSlide::where('active', 1)->orderBy('sort', 'desc')->get()));

        $data['partners'] = Partner::where('active', 1)->orderBy('sort', 'desc')->get();

        $data['settings'] = \App\Models\Setting::first();

        $seo_info = SeoInfo::first();
        $data['seo']['title'] = $seo_info->{'main_page_title'};
        $data['seo']['description'] =$seo_info->{'main_page_description'};
        $data['seo']['keywords'] =  $seo_info->{'main_page_keywords'};

        return view('site.index', compact('data'));
    }

    public function aboutUs(){
        $data['settings'] = \App\Models\Setting::first();

        $seo_info = SeoInfo::first();
        $data['seo']['title'] = $seo_info->{'about_us_page_title'};
        $data['seo']['description'] =$seo_info->{'about_us_page_description'};
        $data['seo']['keywords'] =  $seo_info->{'about_us_page_keywords'};

        return view('site.about-us', compact('data'));
    }

    public function contact(){
        $data = [];
        $data['settings'] = \App\Models\Setting::first();

        $seo_info = SeoInfo::first();
        $data['seo']['title'] = $seo_info->{'contact_page_title'};
        $data['seo']['description'] =$seo_info->{'contact_page_description'};
        $data['seo']['keywords'] =  $seo_info->{'contact_page_keywords'};

        return view('site.contact', compact('data'));
    }

}
