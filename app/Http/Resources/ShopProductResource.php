<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->{'name'},
            'short_description' => $this->{'short_description'},
            'description' => $this->{'description'},
            'price' => number_format($this->price, 0, '', ' '),
            'link' => $this->{'link'},
            'img' => $this->img,
            'img_tmb' => tmbPath($this->img),
        ];
    }
}
