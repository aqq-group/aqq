<?php

function resourceData($data) {
    return json_decode(json_encode($data));
}

function cleanPhone($phone){
    $chars = '!#+() ';
    return preg_replace('/['.$chars.']/', '', $phone);
}

function tmbPath($img){
    return str_replace('uploads/', 'uploads/tmb/', $img);
}

function vardump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

function priceFormat($val) {
    return number_format($val, 0, '', ' ');
}

/*Cart*/
function cartTotal($items){
    $count = 0; $price = 0; $total_price = 0;
    foreach ($items as $item){
        if ($item['table'] == 'hams'){
            $count = $item['count'];
            $price = $count * $item['options']['size_data']['price'];
            $price += $count * $item['options']['package_data']['price'];
            $price += $count * $item['options']['stands_data']['price'];
        }else if ($item['table'] == 'delicacies'){
            $count = $item['count'];
            $price = $count * $item['product']['price'];
        }

        $total_price+= $price;
    }

    return priceFormat($total_price);
}
