<?php

namespace App\Traits;

use Backpack\CRUD\app\Models\Traits\HasEnumFields;
use Backpack\CRUD\app\Models\Traits\HasFakeFields;
use Backpack\CRUD\app\Models\Traits\HasIdentifiableAttribute;
use Backpack\CRUD\app\Models\Traits\HasRelationshipFields;
use Backpack\CRUD\app\Models\Traits\HasTranslatableFields;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


trait CustomCrudTrait
{
    use HasIdentifiableAttribute;
    use HasEnumFields;
    use HasRelationshipFields;
    use CustomHasUploadFields;
    use HasFakeFields;
    use HasTranslatableFields;

    //Getters

    //Mutators
    public function setImgAttribute($value, $tmb_width = null, $attribute_name = 'img')
    {
        if(!$tmb_width){
            $tmb_width = 1000;
        }

        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads";

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            Storage::disk($disk)->delete('public/' . $this->{$attribute_name});
            Storage::disk($disk)->delete('public/' . str_replace('uploads/', 'uploads/tmb/', $this->{$attribute_name}));

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (Str::startsWith($value, 'data:image'))
        {
            $pos  = strpos($value, ';');
            $type = explode('/', substr($value, 0, $pos))[1];
            // 0. Make the image
            $image = Image::make($value)->encode(null, 90);

            // 1. Generate a filename.
            $filename = md5($value.time()) . '.' . $type;

            // 2. Store the image on disk.
            Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

            $image_thumbnail = Image::make($value);
            $image_thumbnail->resize($tmb_width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk($disk)->put($destination_path . '/tmb/'. $filename, $image_thumbnail->stream());

            // 3. Delete the previous image, if there was one.
            Storage::disk($disk)->delete('public/' . $this->{$attribute_name});
            Storage::disk($disk)->delete('public/' . str_replace('uploads/', 'uploads/tmb/', $this->{$attribute_name}));

            // 4. Save the public path to the database
            // but first, remove "public/" from the path, since we're pointing to it
            // from the root folder; that way, what gets saved in the db
            // is the public URL (everything that comes after the domain name)
            //$public_destination_path = Str::replaceFirst('public/', '', $destination_path);
            $this->attributes[$attribute_name] = 'uploads/' . $filename;
        }
    }

    public function setFileAttribute($value, $attribute_name = 'img')
    {
        $disk = config('backpack.base.root_disk_name');
        $destination_path = "public/uploads";

        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }

    public static function hasCrudTrait()
    {
        return true;
    }
}
